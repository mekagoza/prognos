CREATE TABLE roles (
    id SERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE users (
  id SERIAL NOT NULL PRIMARY KEY,
  username TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL,
  promoter INTEGER,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  role_id INTEGER NOT NULL REFERENCES roles(id)
);

INSERT INTO roles (name) VALUES
    ('admin'),
    ('judge'),
    ('regular')
;

CREATE TABLE claims (
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    expires TIMESTAMP WITH TIME ZONE NOT NULL,
    description TEXT NOT NULL,
    creator INTEGER NOT NULL REFERENCES users(id),
    approved BOOLEAN NOT NULL DEFAULT FALSE,
    result BOOLEAN,
    bday TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE orders (
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL REFERENCES users(id),
    side INTEGER NOT NULL,
    claim_id INTEGER NOT NULL REFERENCES claims(id),
    price NUMERIC NOT NULL,
    num_shares INTEGER NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    rank INTEGER NOT NULL
);

CREATE TABLE portfolios (
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL REFERENCES users(id),
    balance NUMERIC NOT NULL
);

CREATE TABLE locks (
    id SERIAL PRIMARY KEY NOT NULL,
    portfolio_id INTEGER NOT NULL REFERENCES portfolios(id),
    order_id INTEGER NOT NULL REFERENCES orders(id),
    amount NUMERIC NOT NULL
);

CREATE TABLE coupons (
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL REFERENCES users(id),
    claim_id INTEGER NOT NULL REFERENCES claims(id),
    shares INTEGER NOT NULL,
    side INTEGER NOT NULL
);

CREATE TABLE trades (
    id SERIAL PRIMARY KEY NOT NULL,
    buyer INTEGER NOT NULL REFERENCES users(Id),
    seller INTEGER NOT NULL REFERENCES users(id),
    claim_id INTEGER NOT NULL REFERENCES claims(id),
    price NUMERIC NOT NULL,
    shares INTEGER NOT NULL,
    created_at TIMESTAMP with TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE OR REPLACE FUNCTION proc_users_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
    -- Hash the password with a newly generated salt
    -- crypt() will store the hash and salt (and the algorithm and iterations) in the column
    new.password := crypt(new.password, gen_salt('bf', 8));
  return new;
end
$$;


--
-- TOC entry 235 (class 1255 OID 16608)
-- Name: proc_users_update(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE OR REPLACE FUNCTION proc_users_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  IF new.password = NULL THEN
    new.password := old.password;
  ELSE
    new.password := crypt(new.password, old.password);
  END IF;
  return new;
end
$$;

--
-- TOC entry 2727 (class 2620 OID 16631)
-- Name: users trigger_users_insert; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trigger_users_insert BEFORE INSERT ON users FOR EACH ROW EXECUTE PROCEDURE proc_users_insert();


--
-- TOC entry 2728 (class 2620 OID 16632)
-- Name: users trigger_users_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER trigger_users_update BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE proc_users_update();

-- TODO FOR DEV ONLY
INSERT INTO users (username, password, promoter, created_at, role_id) VALUES
  ('admin', 'password', null, '2017-09-08 13:00:26.353041', 1),
  ('mark', 'hunter2', 1, '2017-09-08 13:00:28.353041', 2),
  ('ted', 'notpassword', 2, '2017-09-08 13:00:28.353041', 3)
;

INSERT INTO claims (name, expires, description, creator, approved, result, bday) VALUES
    ('Test Claim', '2019-09-08 13:00:28.353041', 'description', 1, false, null, '2017-09-08 13:00:28.353041')
;

INSERT INTO portfolios (user_id, balance) VALUES
  (1, 1000000),
  (2, 1000000),
  (3, 1000000)
;
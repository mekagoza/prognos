use actix_web::http;
use actix_web::{HttpMessage, HttpRequest};
use chrono::prelude::Utc;
use chrono::Duration;
use jswt::{decode, encode, Header, Validation};

use auth::{AuthRole, AuthUser};
use core::model::SendUser;
use routes::ApiError;

// const AUTH_COOKIE: &'static str = "Authorization";

/// Our claims struct, it needs to derive `Serialize` and/or `Deserialize`
#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    // issued at
    iat: i64,
    // expiration
    exp: i64,
    user: SendUser,
    role: i32,
}

#[derive(Clone)]
pub struct Authorizer {
    key: String,
}

impl Authorizer {
    pub fn new(key: String) -> Authorizer {
        Authorizer { key }
    }

    pub fn generate(&self, user: SendUser, role: i32) -> String {
        let now = Utc::now();
        let next_week = now + Duration::weeks(1);
        let now_sec = now.timestamp();

        let payload = Claims {
            iat: now_sec,
            exp: next_week.timestamp(),
            user: user,
            role: role,
        };

        encode(&Header::default(), &payload, self.key.as_bytes()).unwrap()
    }

    pub fn verify(&self, token: &str, role: Option<i32>) -> Option<SendUser> {
        let claims = match decode::<Claims>(token, self.key.as_bytes(), &Validation::default()) {
            Ok(claims) => claims,
            _ => {
                return None;
            }
        };

        let now = Utc::now();

        if now.timestamp() > claims.claims.exp {
            return None;
        }

        if role.map_or(false, |role| claims.claims.user.role_id != role) {
            return None;
        }

        return Some(claims.claims.user);
    }

    pub fn auth<S, R: AuthRole>(&self, req: &HttpRequest<S>) -> Result<AuthUser<R>, ApiError> {
        let jwt = match req.headers().get(http::header::AUTHORIZATION) {
            Some(x) => x,
            None => {
                return Err(ApiError::Unauthorized(
                    "Missing 'Authorization' header".into(),
                ));
            }
        };

        if let Some(user) = self.verify(jwt.to_str().expect("Non ASCII header"), R::ID) {
            return Ok(AuthUser(user, R::new()));
        } else {
            return Err(ApiError::Unauthorized(
                "JWT Token verification failed".into(),
            ));
        }
    }
}

use core::model::SendUser;

#[derive(Debug, Clone, Deserialize)]
pub struct AuthUser<T: AuthRole>(pub SendUser, pub T);

pub trait AuthRole {
    const ID: Option<i32>;
    fn new() -> Self;
}

pub struct Admin;
impl AuthRole for Admin {
    const ID: Option<i32> = Some(1);
    fn new() -> Self {
        Admin
    }
}

pub struct Judge;
impl AuthRole for Judge {
    const ID: Option<i32> = Some(2);
    fn new() -> Self {
        Judge
    }
}

pub struct Member;
impl AuthRole for Member {
    const ID: Option<i32> = Some(3);
    fn new() -> Self {
        Member
    }
}

pub struct Any;
impl AuthRole for Any {
    const ID: Option<i32> = None;
    fn new() -> Self {
        Any
    }
}

pub mod claim;
pub mod login;
pub mod order;
pub mod portfolio;
pub mod user;

use actix::MailboxError;
use actix_web::http::StatusCode as Status;
use actix_web::{self, HttpResponse, ResponseError};
use serde_json;

use core;

#[derive(Debug, Fail)]
pub enum ApiError {
    #[fail(display = "Backend Error: {}", _0)]
    Backend(#[cause] core::Error),
    #[fail(display = "JSON (de)serialize error: {}", _0)]
    JsonErr(#[cause] serde_json::Error),
    #[fail(display = "Actix Error: {}", _0)]
    Actix(String),
    #[fail(display = "Unauthorized: {}", _0)]
    Unauthorized(String),
    #[fail(display = "Bad Client Data: {}", _0)]
    BadInput(&'static str),
}

impl ApiError {}

impl ResponseError for ApiError {
    fn error_response(&self) -> HttpResponse {
        let status = match self {
            ApiError::JsonErr(_) | ApiError::Actix(_) | ApiError::Backend(_) => {
                Status::INTERNAL_SERVER_ERROR
            }
            ApiError::Unauthorized(_) => Status::UNAUTHORIZED,
            ApiError::BadInput(_) => Status::UNPROCESSABLE_ENTITY,
        };

        let json = json!({"error": self.to_string()});

        HttpResponse::build(status).json(json)
    }
}

impl From<serde_json::Error> for ApiError {
    fn from(error: serde_json::Error) -> Self {
        ApiError::JsonErr(error)
    }
}

impl From<MailboxError> for ApiError {
    fn from(error: MailboxError) -> Self {
        ApiError::Actix(error.to_string())
    }
}

impl From<actix_web::Error> for ApiError {
    fn from(error: actix_web::Error) -> Self {
        ApiError::Actix(error.to_string())
    }
}

impl From<core::Error> for ApiError {
    fn from(error: core::Error) -> Self {
        ApiError::Backend(error)
    }
}

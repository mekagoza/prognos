use actix_web::{AsyncResponder, FutureResponse, Json, Path, State};
use futures::Future;

use core::model::Portfolio;
use core::portfolio;
use routes::ApiError;
use MyApp;

pub fn portfolio(
    (state, path): (State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Portfolio>, ApiError> {
    let msg = portfolio::Fetch(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let port = res?;

            Ok(Json(port))
        })
        .responder()
}

// #[get("/portfolio/<id_>")]
// fn portfolio(conn: Conn, id_: i32, _log: Log) -> Result<Json<Value>, ApiError> {
//
//     let conn = &*conn;
//
//     let portfolio: Portfolio = portfolios.find(&id_).first(conn)?;
//
//     let val = json!({
//         "status": 200,
//         "portfolio": portfolio
//     });
//
//     Ok(Json(val))
// }

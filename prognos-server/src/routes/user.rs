use actix_web::{AsyncResponder, FutureResponse, Json, Path, Query, State};
use futures::Future;
use serde_json::Value;

use core::model::{Coupon, Order, SendUser, Trade};
use core::user;
use routes::ApiError;
use MyApp;

#[derive(Deserialize)]
pub struct Opts {
    pub portfolio: Option<bool>,
    pub promoter: Option<bool>,
}

pub fn user(
    (state, path, opts): (State<MyApp>, Path<i32>, Query<Opts>),
) -> FutureResponse<Json<Value>, ApiError> {
    let opts = opts.into_inner();

    state
        .db
        .send(user::Get {
            id: *path,
            promoter: opts.promoter.unwrap_or(true),
            portfolio: opts.portfolio.unwrap_or(false),
        })
        .from_err()
        .and_then(move |res| {
            let get_res = res?;

            let json = Json(json!({
                "user": SendUser::from(get_res.user),
                "portfolio": get_res.portfolio,
                "promo": get_res.promoter.map(|u| SendUser::from(u))}));

            Ok(json)
        })
        .responder()
}

pub fn orders(
    (state, path): (State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Vec<Order>>, ApiError> {
    let msg = user::Orders(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let orders = res?;

            Ok(Json(orders))
        })
        .responder()
}

pub fn coupons(
    (state, path): (State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Vec<Coupon>>, ApiError> {
    let msg = user::Coupons { id: *path };

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let data = res?;

            Ok(Json(data))
        })
        .responder()
}

pub fn trades(
    (state, path): (State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Vec<Trade>>, ApiError> {
    let msg = user::Trades(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let data = res?;

            Ok(Json(data))
        })
        .responder()
}

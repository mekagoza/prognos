use actix_web::http;
use actix_web::{AsyncResponder, FutureResponse, HttpRequest, HttpResponse, Json, Responder, State};
use futures::{future, Future};
use serde_json::Value;

use auth::{Any, AuthUser};
use core::model::{Portfolio, SendUser, User};
use core::{auth, user};
use routes::ApiError;
use MyApp;

pub fn login(
    (state, form, req): (State<MyApp>, Json<auth::UserForm>, HttpRequest<MyApp>),
) -> FutureResponse<HttpResponse, ApiError> {
    let login_user = form.into_inner();
    let log = state
        .logger
        .new(o!("username" => login_user.username.clone()));

    state
        .db
        .send(login_user)
        .from_err()
        .and_then(move |res| -> Result<_, ApiError> {
            let user: User = res?;

            let jwt_str = state
                .auth
                .generate(SendUser::from(user.clone()), user.role_id);

            let json = Json(json!({
                    "user": SendUser::from(user),
                    "token": jwt_str.clone(),
                }));

            info!(log, "logged in user");

            let mut resp = json.respond_to(&req)?;

            // let mut resp = resp?;
            resp.headers_mut()
                .insert(http::header::AUTHORIZATION, jwt_str.parse().unwrap());

            Ok(resp)
        })
        .responder()
}

pub fn user(
    (state, req): (State<MyApp>, HttpRequest<MyApp>),
) -> FutureResponse<Json<SendUser>, ApiError> {
    let auth_user: AuthUser<Any> = match state.auth.auth(&req) {
        Ok(u) => u,
        Err(e) => {
            return Box::new(future::err(e.into()));
        }
    };

    state
        .db
        .send(user::Fetch(auth_user.0.id))
        .from_err()
        .and_then(move |user| {
            let user = user?;
            let json = Json(SendUser::from(user));

            Ok(json)
            // Ok(json.respond_to(&req)?)
        })
        .responder()
}

pub fn delete(
    (state, req): (State<MyApp>, HttpRequest<MyApp>),
) -> FutureResponse<Json<Value>, ApiError> {
    let auth_user: AuthUser<Any> = match state.auth.auth(&req) {
        Ok(u) => u,
        Err(e) => {
            return Box::new(future::err(e.into()));
        }
    };

    state
        .db
        .send(auth::Delete(auth_user.0.id))
        .from_err()
        .and_then(move |res| {
            let _ = res?;
            info!(state.logger, "user deleted"; o!("username" => auth_user.0.username));

            Ok(Json(json!({
                "status": 200, 
                "msg": "deletion successful"
            })))
        })
        .responder()
}

pub fn signup(
    (state, form, req): (State<MyApp>, Json<auth::RegisterForm>, HttpRequest<MyApp>),
) -> FutureResponse<HttpResponse, ApiError> {
    let signup_user = form.into_inner();
    let log = state
        .logger
        .new(o!("username" => signup_user.username.clone()));

    state
        .db
        .send(signup_user)
        .from_err()
        .and_then(move |res| {
            let (user, portfolio): (User, Portfolio) = res?;

            let jwt_str = state
                .auth
                .generate(SendUser::from(user.clone()), user.role_id);

            let json = Json(json!({
                    "user": SendUser::from(user),
                    "token": jwt_str.clone(),
                    "portfolio": portfolio,
                }));

            info!(log, "registered new user");

            let mut resp = json.respond_to(&req)?;

            resp.headers_mut()
                .insert(http::header::AUTHORIZATION, jwt_str.parse().unwrap());

            Ok(resp)
        })
        .responder()
}

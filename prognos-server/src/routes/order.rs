use actix_web::{AsyncResponder, FutureResponse, HttpRequest, Json, Path, State};
use futures::{future, Future};
use serde_json::Value;

use auth::{Any, AuthUser};
use core::model::Order;
use core::order::{self, OrderRes};
use routes::ApiError;
use MyApp;

pub fn fetch_order(
    (state, path): (State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Order>, ApiError> {
    let msg = order::Fetch(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(|res| {
            let order = res?;

            Ok(Json(order))
        })
        .responder()
}

pub fn cancel_order(
    (state, path): (State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Value>, ApiError> {
    let msg = order::Cancel(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(|res| {
            let _ = res?;

            Ok(Json(json!({"status": "cancelled"})))
        })
        .responder()
}

pub fn place_order(
    (state, req, path, data): (
        State<MyApp>,
        HttpRequest<MyApp>,
        Path<i32>,
        Json<order::Place>,
    ),
) -> FutureResponse<Json<Value>, ApiError> {
    let new_order = data.into_inner();
    let claim_id = *path;

    let author: AuthUser<Any> = match state.auth.auth(&req) {
        Ok(u) => u,
        Err(e) => {
            return Box::new(future::err(e.into()));
        }
    };

    if new_order.claim_id != claim_id {
        warn!(state.logger, "Fraudulent order");
        return Box::new(future::err(ApiError::BadInput("Mismatched Claim ID")));
    }

    if new_order.user_id != author.0.id {
        warn!(state.logger, "Fraudulent order");
        return Box::new(future::err(ApiError::BadInput("Mismatched Author ID")));
    }

    state
        .db
        .send(new_order)
        .from_err()
        .and_then(move |res| {
            let order_res = res?;

            Ok(match order_res {
                OrderRes::Complete(trade) => Json(json!({
                        "trade": trade,
                        "status": "complete",
                    })),
                OrderRes::Pending(order) => Json(json!({
                        "order": order,
                        "status": "pending",
                    })),
            })
        })
        .responder()
}

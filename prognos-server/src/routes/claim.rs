use actix_web::{AsyncResponder, FutureResponse, HttpRequest, Json, Path, State};
use futures::{future, Future};
use serde_json::Value;

use auth::{Admin, Any, AuthUser};
use core::claim;
use core::model::{Claim, Order, Trade};
use routes::ApiError;
use MyApp;

pub fn claim((state, path): (State<MyApp>, Path<i32>)) -> FutureResponse<Json<Value>, ApiError> {
    let msg = claim::Fetch(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(|res| {
            let (claim, creator_name) = res?;

            let json = json!({
                "claim": claim,
                "creator_name": creator_name,
            });
            Ok(Json(json))
        })
        .responder()
}

pub fn all(state: State<MyApp>) -> FutureResponse<Json<Vec<(Claim, String)>>, ApiError> {
    let msg = claim::All;

    state
        .db
        .send(msg)
        .from_err()
        .and_then(|res| {
            let claims = res?;

            Ok(Json(claims))
        })
        .responder()
}

pub fn orders(
    (state, path): (State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Vec<Order>>, ApiError> {
    let msg = claim::Orders(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let orders = res?;

            Ok(Json(orders))
        })
        .responder()
}

pub fn trades(
    (state, path): (State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Vec<Trade>>, ApiError> {
    let msg = claim::Trades(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let data = res?;

            Ok(Json(data))
        })
        .responder()
}

pub fn approve(
    (req, state, path): (HttpRequest<MyApp>, State<MyApp>, Path<i32>),
) -> FutureResponse<Json<Value>, ApiError> {
    let _: AuthUser<Admin> = match state.auth.auth(&req) {
        Ok(u) => u,
        Err(e) => {
            return Box::new(future::err(e.into()));
        }
    };

    let msg = claim::Approve(*path);

    state
        .db
        .send(msg)
        .from_err()
        .and_then(move |res| {
            let _ = res?;

            Ok(Json(json!({
                "status": 200,
                "error": ()
                })))
        })
        .responder()
}

#[derive(Deserialize)]
pub struct ClaimForm {
    name: String,
    description: String,
    expires: String,
}

pub fn create(
    (state, data, req): (State<MyApp>, Json<ClaimForm>, HttpRequest<MyApp>),
) -> FutureResponse<Json<Value>, ApiError> {
    let author: AuthUser<Any> = match state.auth.auth(&req) {
        Ok(u) => u,
        Err(e) => {
            return Box::new(future::err(e.into()));
        }
    };

    let data = data.into_inner();

    let msg = claim::Create {
        name: data.name,
        description: data.description,
        expires: data.expires,
        author: author.0.id,
    };

    state
        .db
        .send(msg)
        .from_err()
        .and_then(|res| {
            let (claim, creator) = res?;
            let json = json!({
                "claim": claim,
                "creator": creator,
            });

            Ok(Json(json))
        })
        .responder()
}

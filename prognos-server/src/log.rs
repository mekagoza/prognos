use std::ops::Deref;

use actix_web::middleware::{Finished, Middleware, Started};
use actix_web::{HttpRequest, HttpResponse, Result};
use slog::{Drain, Duplicate, Logger};
use slog_async;
// use slog_json;
use slog_term;

#[derive(Clone)]
pub struct Log(pub Logger);

impl Deref for Log {
    type Target = Logger;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<S> Middleware<S> for Log {
    fn start(&self, req: &mut HttpRequest<S>) -> Result<Started> {
        let uri = req.uri().to_string();
        let method = req.method().to_string();
        info!(self.0, "request"; "uri" => uri.clone(), "method" => method);

        Ok(Started::Done)
    }

    fn finish(&self, req: &mut HttpRequest<S>, resp: &HttpResponse) -> Finished {
        let uri = req.uri().to_string();
        let method = req.method().to_string();
        let status = resp.status().to_string();
        info!(self.0, "response"; "uri" => uri.clone(), "method" => method, "status" => status);

        Finished::Done
    }
}

pub fn log(log_path: &str) -> Logger {
    use std::fs::OpenOptions;

    let file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(log_path)
        .expect("Error opening log file");

    let decorator = slog_term::PlainDecorator::new(file);
    let d1 = slog_term::FullFormat::new(decorator).build().fuse();
    let d1 = slog_async::Async::new(d1).build().fuse();

    let decorator = slog_term::TermDecorator::new().build();
    let d2 = slog_term::FullFormat::new(decorator).build().fuse();
    let d2 = slog_async::Async::new(d2).build().fuse();

    let drain = Duplicate::new(d1, d2).fuse();

    let root = Logger::root(drain, o!());

    root
}

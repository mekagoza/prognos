extern crate actix;
extern crate actix_web;
extern crate dotenv;
extern crate prognos;

use std::collections::HashMap;

use actix_web::server;

fn main() {
    let env: HashMap<String, String> = dotenv::vars().collect();
    let (host, port): (String, String) = (
        env.get("HOST").cloned().unwrap_or("127.0.0.1".into()),
        env.get("PORT").cloned().unwrap_or("8000".into()),
    );

    let port = port.parse().unwrap_or(8000);

    let sys = actix::System::new("prognos");

    server::new(move || prognos::build(env.clone()))
        .bind((host.as_str(), port))
        .expect("Error launching Actix Server")
        .start();

    let _ = sys.run();
}

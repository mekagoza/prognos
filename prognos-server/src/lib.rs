extern crate actix;
extern crate actix_web;
extern crate bigdecimal;
extern crate chrono;
extern crate dotenv;
#[macro_use]
extern crate failure;
extern crate futures;
extern crate jsonwebtoken as jswt;
extern crate prognos_core as core;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_json;
extern crate slog_term;

mod auth;
mod jwt;
mod log;
mod routes;

use std::collections::HashMap;

use actix::prelude::*;
use actix_web::http::Method;
use actix_web::middleware::cors::Cors;
use actix_web::App;
use slog::Logger;

use core::{init_core, Core, PgPool};
use jwt::Authorizer;
use routes::{claim, login, order, portfolio, user};

pub const JWT_KEY: &'static str = "JWT_KEY";
pub const DB_KEY: &'static str = "DATABASE_URL";
pub const TEST_DB_KEY: &'static str = "TEST_DATABASE_URL";
pub const LOG_KEY: &'static str = "LOG_PATH";
pub const ORIGINS_KEY: &'static str = "ALLOWED_ORIGINS";

#[derive(Clone)]
pub struct MyApp {
    #[allow(unused)]
    logger: Logger,
    auth: Authorizer,
    db: Addr<Syn, Core>,
}

pub fn build(vars: HashMap<String, String>) -> App<MyApp> {
    let db_url = vars.get(DB_KEY).expect("DATABASE_URL must be set");
    let db_pool = match init_core(&db_url) {
        Ok(pool) => pool,
        Err(e) => panic!("Error initializing database pool: {}", e),
    };

    let jwt_key = vars.get(JWT_KEY).expect("JWT_KEY must be set").clone();

    let log_path = vars.get(LOG_KEY).expect("LOG_PATH must be set");
    let logger = log::log(log_path);

    let origin = vars.get(ORIGINS_KEY).expect("ALLOWED_ORIGINS must be set");
    let cors = cors(origin);

    let auth = Authorizer::new(jwt_key);

    build_app(db_pool, auth, cors, logger)
}

fn build_app(pool: PgPool, auth: Authorizer, cors: Cors, logger: Logger) -> App<MyApp> {
    let core_logger = logger.new(o!("Context" => "Core"));

    let addr = SyncArbiter::start(3, move || Core::new(core_logger.clone(), pool.clone()));

    let state = MyApp {
        db: addr,
        auth: auth,
        logger: logger.clone(),
    };

    App::with_state(state)
        .prefix("/api")
        .middleware(log::Log(logger))
        .middleware(cors)
        .resource("/auth/login", |r| r.method(Method::POST).with(login::login))
        .resource("/auth/signup", |r| {
            r.method(Method::POST).with(login::signup)
        })
        .resource("/auth/user", |r| r.method(Method::GET).with(login::user))
        .resource("/auth/delete", |r| {
            r.method(Method::DELETE).with(login::delete)
        })
        .resource("/user/{uid}", |r| r.method(Method::GET).with(user::user))
        .resource("/user/{id}/orders", |r| {
            r.method(Method::GET).with(user::orders)
        })
        .resource("/user/{id}/coupons", |r| {
            r.method(Method::GET).with(user::coupons)
        })
        .resource("/user/{id}/trades", |r| {
            r.method(Method::GET).with(user::trades)
        })
        .resource("/portfolio/{id}", |r| {
            r.method(Method::GET).with(portfolio::portfolio)
        })
        .resource("/claim/all", |r| r.method(Method::GET).with(claim::all))
        .resource("/claim/{id}", |r| r.method(Method::GET).with(claim::claim))
        .resource("/claim/{id}/approve", |r| {
            r.method(Method::POST).with(claim::approve)
        })
        .resource("/claim/{id}/orders", |r| {
            r.method(Method::GET).with(claim::orders)
        })
        .resource("/claim/{id}/trades", |r| {
            r.method(Method::GET).with(claim::trades)
        })
        .resource("/claim", |r| r.method(Method::POST).with(claim::create))
        .resource("/claim/{id}/order", |r| {
            r.method(Method::POST).with(order::place_order)
        })
        .resource("/order/{id}", |r| {
            r.method(Method::GET).with(order::fetch_order)
        })
        .resource("/order/{id}", |r| {
            r.method(Method::DELETE).with(order::cancel_order)
        })
}

fn cors(origin: &str) -> Cors {
    Cors::build()
        .allowed_origin(origin)
        .supports_credentials()
        .finish()
}

extern crate actix;
extern crate actix_web;
extern crate dotenv;
extern crate futures;
extern crate prognos;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate fake;

use std::collections::HashMap;

use actix_web::http::StatusCode as Status;
use actix_web::test::TestServer;
use actix_web::{http, HttpMessage};
use futures::future::Future;
use serde_json::Value;

const ORIGIN: &'static str = "test-client";

fn setup() -> TestServer {
    let mut vars: HashMap<_, _> = dotenv::vars().collect();
    let test_db_url = vars.get(prognos::TEST_DB_KEY)
        .expect("missing test db url")
        .clone();

    vars.insert(prognos::DB_KEY.to_string(), test_db_url);
    vars.insert(prognos::ORIGINS_KEY.to_string(), ORIGIN.to_string());

    TestServer::with_factory(move || prognos::build(vars.clone()))
}

#[test]
fn register() {
    let mut server = setup();

    // Register
    let pass = fake!(Internet.password(8, 200));

    let formdata = json!({
            "username" : fake!(Internet.user_name),
            "password" : pass,
            "confirm": pass,
            "promoter": "admin"
        });

    let req = server
        .client(http::Method::POST, "/api/auth/signup")
        .header("Origin", ORIGIN)
        .json(formdata)
        .expect("req");

    let response = server.execute(req.send()).expect("resp");

    // assert_eq!(response.status(), Status::OK);
    let bs = response.body().wait().expect("Bad Body");

    let s = ::std::str::from_utf8(&bs).unwrap_or_default();

    println!("Body = {}", s);

    let value: Value = serde_json::from_slice(&bs).expect("Bad JSON");

    let token = value["token"].as_str().expect("token malformed").to_owned();

    // Check auth
    let req = server
        .client(http::Method::GET, "/api/auth/user")
        .header("Authorization", token)
        .header("Origin", ORIGIN)
        .finish()
        .expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);

    let body = response.body().wait().expect("Bad body");
    assert!(!body.is_empty());
}

#[test]
fn register_and_login() {
    let mut server = setup();

    // Register
    let (uname, pass) = (fake!(Internet.user_name), fake!(Internet.password(8, 200)));
    let formdata = json!({
            "username" : &uname,
            "password" : &pass,
            "confirm": &pass,
            "promoter": "admin"
        });

    let req = server
        .client(http::Method::POST, "/api/auth/signup")
        .header("Origin", ORIGIN)
        .json(formdata)
        .expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);

    // Check Login
    let formdata = json!({
            "username" : &uname,
            "password" : &pass,
        });

    let req = server
        .client(http::Method::POST, "/api/auth/login")
        .header("Origin", ORIGIN)
        .json(formdata)
        .expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);
    let bs = response.body().wait().expect("Bad Body");

    let s = ::std::str::from_utf8(&bs).unwrap_or_default();

    println!("Body = {}", s);

    let value: Value = serde_json::from_slice(&bs).expect("Bad JSON");

    let token = value["token"].as_str().expect("token malformed").to_owned();

    // Check auth
    let req = server
        .client(http::Method::GET, "/api/auth/user")
        .header("Authorization", token)
        .header("Origin", ORIGIN)
        .finish()
        .expect("req");

    let response = server.execute(req.send()).expect("resp");

    assert_eq!(response.status(), Status::OK);

    let body = response.body().wait().expect("Bad body");
    assert!(!body.is_empty());
}

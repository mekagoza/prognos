# Prognos
Prognos is a predictive market engine written in [Rust](https://www.rust-lang.org/en-US/index.html)

## Description
The project is divided into a frontend app using [Vue.js](https://vuejs.org/). The backend is a Rust web-server using [Actix Web](https://actix.rs/) and [Diesel](http://diesel.rs/).

## Setup
First ensure the aformentioned tools/platforms are installed. NPM and Node generally have good system packages, and Rust can easily be installed from [here](https://www.rust-lang.org/en-US/install.html)

## Database
This app uses PostgreSQL, so that must be installed and the database daemon running. In addition, the `diesel` CLI tool requires the libraries for PostgreSQL, MySQL, and SQLite to all be present, as it manages projects using any of them.

You must create at least one database for the app. First create a postgresql database for this project.
Then create a file called '.env' by copying the example.env document
```bash
   	$ cp example.env .env
```
Then enter the `DATABASE_URL`, as well as the `HOST`, `PORT`, `LOG_PATH`, and `JWT_KEY` entries. `HOST` and `PORT` default to `127.0.0.1:8000`, every other environment variable is required. `TEST_DATABASE_URL` is only required if you wish to be able to run the tests. `ALLOW_ORIGIN` is also required, for [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) handling. Assuming development, this will probably be something like `http://localhost:1234`

Install the Diesel CLI tool:
```bash
	$ cargo install diesel_cli
```
Once this is complete (it may take a while), run
```bash
	$ diesel setup
	$ diesel migration run
```

## Running
Now you should be able to run
```bash
	$ cargo check # Just checks types, memory safety, etc.
	$ cargo build # build executable
	$ cargo run   # run it, ready to take connections.
```

## Features
- [x] registration
- [x] login
- [x] listing claims and viewing individual ones
- [x] creating new claims
- [x] basic user profile
- [x] basic trading 
- [ ] extended user profile with trading information, history, and coupons
- [ ] extended claim information including view trading history, volume, historical price averages, etc.
- [ ] judging claims complete and paying out winnings

The above list is incomplete and subject to change at any time!

## Contribute
 
Contributions are welcome!

## License

[LICENSE-APACHE](https://github.com/OUIRC/Rust-webapp-starter/blob/master/LICENSE).

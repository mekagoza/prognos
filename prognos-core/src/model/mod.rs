pub mod claim;
pub mod coupon;
pub mod lock;
pub mod order;
pub mod portfolio;
pub mod trade;
pub mod user;

pub use self::claim::{Claim, NewClaim};
pub use self::coupon::{Coupon, NewCoupon};
pub use self::lock::{Lock, NewLock};
pub use self::order::{NewOrder, Order};
pub use self::portfolio::{NewPortfolio, Portfolio};
pub use self::trade::{NewTrade, Trade};
pub use self::user::{NewUser, SendUser, User};

use std::io::Write;
use std::ops::Not;

use diesel::backend::Backend;
use diesel::deserialize::{FromSql, Result as DDRes};
use diesel::serialize::{Output, Result as DRes, ToSql};
use diesel::sql_types::Integer;
use failure::Fail;

use Error;

#[derive(Clone, FromSqlRow, AsExpression, Debug, Copy, Serialize, Deserialize, PartialEq, Eq)]
#[sql_type = "Integer"]
pub enum Side {
    Ask,
    Bid,
}

impl Not for Side {
    type Output = Side;

    fn not(self) -> Self::Output {
        match self {
            Side::Bid => Side::Ask,
            Side::Ask => Side::Bid,
        }
    }
}

impl<DB: Backend> ToSql<Integer, DB> for Side {
    fn to_sql<W: Write>(&self, out: &mut Output<W, DB>) -> DRes {
        match *self {
            Side::Ask => <i32 as ToSql<Integer, DB>>::to_sql(&0, out),
            Side::Bid => <i32 as ToSql<Integer, DB>>::to_sql(&1, out),
        }
    }
}

impl<DB: Backend<RawValue = [u8]>> FromSql<Integer, DB> for Side {
    fn from_sql(bytes: Option<&DB::RawValue>) -> DDRes<Side> {
        let val = <i32 as FromSql<Integer, DB>>::from_sql(bytes)?;
        // let val = i32::from_sql(bytes)?;

        match val {
            0 => Ok(Side::Ask),
            1 => Ok(Side::Bid),
            _ => Err(Box::new(
                Error::BadInput("Malformed Side stored in database").compat(),
            )),
        }
    }
}

use bigdecimal::BigDecimal;
use chrono::prelude::{DateTime, Utc};

use model::Claim;
use schema::trades;

#[derive(Associations, Serialize, Debug, Clone, PartialEq, Identifiable, Queryable)]
#[table_name = "trades"]
#[belongs_to(Claim, foreign_key = "claim_id")]
pub struct Trade {
    pub id: i32,
    pub buyer: i32,
    pub seller: i32,
    pub claim_id: i32,
    pub price: BigDecimal,
    pub shares: i32,
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Insertable)]
#[table_name = "trades"]
pub struct NewTrade {
    pub buyer: i32,
    pub seller: i32,
    pub claim_id: i32,
    pub price: BigDecimal,
    pub shares: i32,
}

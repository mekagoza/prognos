use bigdecimal::BigDecimal;

use model::User;
use schema::portfolios;

#[derive(Associations, Serialize, Deserialize, Debug, Clone, PartialEq, Identifiable, Queryable)]
#[table_name = "portfolios"]
#[belongs_to(User, foreign_key = "user_id")]
pub struct Portfolio {
    pub id: i32,
    pub user_id: i32,
    pub balance: BigDecimal,
}

#[derive(Debug, Insertable)]
#[table_name = "portfolios"]
pub struct NewPortfolio {
    pub user_id: i32,
    pub balance: BigDecimal,
}

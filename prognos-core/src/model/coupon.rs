use model::{Claim, Side, User};
use schema::coupons;

#[derive(Associations, Debug, Clone, PartialEq, Identifiable, Queryable, Serialize)]
#[table_name = "coupons"]
#[belongs_to(User, foreign_key = "user_id")]
#[belongs_to(Claim, foreign_key = "claim_id")]
pub struct Coupon {
    pub id: i32,
    pub user_id: i32,
    pub claim_id: i32,
    pub shares: i32,
    pub side: Side,
}

#[derive(Debug, Insertable)]
#[table_name = "coupons"]
pub struct NewCoupon {
    pub user_id: i32,
    pub claim_id: i32,
    pub shares: i32,
    pub side: Side,
}

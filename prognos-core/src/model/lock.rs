use bigdecimal::BigDecimal;

use model::{Order, Portfolio};
use schema::locks;

#[derive(Associations, Debug, Clone, PartialEq, Identifiable, Queryable, Serialize)]
#[table_name = "locks"]
#[belongs_to(Portfolio, foreign_key = "portfolio_id")]
#[belongs_to(Order, foreign_key = "order_id")]
pub struct Lock {
    pub id: i32,
    pub order_id: i32,
    pub portfolio_id: i32,
    pub amount: BigDecimal,
}

#[derive(Debug, Insertable)]
#[table_name = "locks"]
pub struct NewLock {
    pub order_id: i32,
    pub portfolio_id: i32,
    pub amount: BigDecimal,
}

use bigdecimal::BigDecimal;
use chrono::prelude::{DateTime, Utc};

use model::{Claim, Side, User};
use schema::orders;

#[derive(Associations, Debug, Clone, PartialEq, Identifiable, Queryable, Serialize)]
#[table_name = "orders"]
#[belongs_to(User, foreign_key = "user_id")]
#[belongs_to(Claim, foreign_key = "claim_id")]
pub struct Order {
    pub id: i32,
    pub user_id: i32,
    pub side: Side,
    pub claim_id: i32,
    pub price: BigDecimal,
    pub num_shares: i32,
    pub created_at: DateTime<Utc>,
    pub rank: i32,
}

#[derive(Debug, Insertable)]
#[table_name = "orders"]
pub struct NewOrder {
    pub user_id: i32,
    pub side: Side,
    pub claim_id: i32,
    pub price: BigDecimal,
    pub num_shares: i32,
    pub rank: i32,
}

use chrono::{DateTime, Utc};
use model::user::User;
use schema::claims;

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Queryable, Associations)]
#[belongs_to(User, foreign_key = "creator")]
pub struct Claim {
    pub id: i32,
    pub name: String,
    pub expires: DateTime<Utc>,
    pub description: String,
    pub creator: i32,
    pub approved: bool,
    pub result: Option<bool>,
    pub bday: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
#[table_name = "claims"]
pub struct NewClaim<'a> {
    pub name: &'a str,
    pub expires: DateTime<Utc>,
    pub description: &'a str,
    pub creator: i32,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ClaimNew {
    pub creator: i32,
    pub name: String,
    pub expires: DateTime<Utc>,
    pub description: String,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ClaimId {
    pub claim_id: i32,
}

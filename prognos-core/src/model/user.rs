use chrono::{DateTime, Utc};
use schema::users;

#[derive(Debug, Clone, PartialEq, Identifiable, Queryable)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub password: String,
    pub promoter: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub role_id: i32,
}

#[derive(Debug, Serialize, Deserialize, Insertable)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub password: &'a str,
    pub promoter: Option<i32>,
    pub role_id: i32,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SendUser {
    pub id: i32,
    pub username: String,
    pub promoter: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub role_id: i32,
}

impl From<User> for SendUser {
    fn from(user: User) -> SendUser {
        SendUser {
            id: user.id,
            username: user.username,
            promoter: user.promoter,
            created_at: user.created_at,
            role_id: user.role_id,
        }
    }
}

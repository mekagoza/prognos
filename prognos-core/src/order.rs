use std::cmp::max;
use std::convert::Into;
use std::str::FromStr;

use actix::prelude::*;
use bigdecimal::BigDecimal;
use diesel;
use diesel::dsl::sum;
use diesel::prelude::*;

use model::{Claim, Coupon, Lock, NewCoupon, NewLock, NewOrder, NewTrade, Order, Portfolio, Side,
            Trade, User};
use {Error, Result};

use super::Core;

#[derive(Debug, Clone)]
pub struct Fetch(pub i32);

#[derive(Debug, Clone)]
pub struct Cancel(pub i32);

#[derive(Debug, Clone, Deserialize)]
pub struct Place {
    pub user_id: i32,
    pub side: String,
    pub claim_id: i32,
    pub price: String,
    pub num_shares: i32,
    pub rank: i32,
}

#[derive(Debug, Clone)]
pub enum OrderRes {
    Complete(Trade),
    Pending(Order),
}

impl Place {
    pub fn to_new_order(&self) -> Option<NewOrder> {
        let side: Side = match &*self.side {
            "ask" | "Ask" => Some(Side::Ask),
            "bid" | "Bid" => Some(Side::Bid),
            _ => None,
        }?;

        let price = BigDecimal::from_str(&self.price).ok()?;

        let no = NewOrder {
            user_id: self.user_id,
            claim_id: self.claim_id,
            price: price,
            num_shares: self.num_shares,
            rank: 0,
            side: side,
        };

        Some(no)
    }
}

impl Message for Fetch {
    type Result = Result<Order>;
}

impl Message for Cancel {
    type Result = Result<()>;
}

impl Message for Place {
    type Result = Result<OrderRes>;
}

impl Handler<Fetch> for Core {
    type Result = Result<Order>;

    fn handle(&mut self, f: Fetch, _: &mut Self::Context) -> Result<Order> {
        use schema::orders::dsl::*;

        let conn = self.conn()?;

        let order = orders.find(f.0).first(&conn)?;

        Ok(order)
    }
}

impl Handler<Cancel> for Core {
    type Result = Result<()>;

    fn handle(&mut self, f: Cancel, _: &mut Self::Context) -> Result<()> {
        use schema::orders::dsl::*;
        use schema::{locks, portfolios};

        let conn = self.conn()?;

        let order: Order = orders.find(f.0).first(&conn)?;

        // Unlock any locked funds
        let port_id: i32 = portfolios::table
            .filter(portfolios::user_id.eq(order.user_id))
            .select(portfolios::id)
            .first(&conn)?;

        let lock: Option<Lock> = locks::table
            .filter(
                locks::order_id
                    .eq(order.id)
                    .and(locks::portfolio_id.eq(port_id)),
            )
            .first(&conn)
            .optional()?;

        conn.transaction::<(), Error, _>(|| {
            if let Some(lock) = lock {
                diesel::delete(&lock).execute(&conn)?;
            }

            diesel::delete(orders.find(f.0)).execute(&conn)?;

            Ok(())
        })?;

        Ok(())
    }
}

impl Handler<Place> for Core {
    type Result = Result<OrderRes>;

    fn handle(&mut self, msg: Place, _: &mut Self::Context) -> Result<OrderRes> {
        use diesel::pg::PgConnection;
        use schema::{claims, coupons, locks, orders, portfolios, trades, users};

        let conn = self.conn()?;
        let conn = &conn;

        let author: User = users::table.find(msg.user_id).first(conn)?;
        // Make sure claim exists
        let cr: Option<Claim> = claims::table.find(msg.claim_id).first(conn).optional()?;

        if let None = cr {
            return Err(Error::BadInput("No such claim"));
        };

        let log = self.log
            .new(o!("author" => author.username, "author_id" => author.id));

        let new = msg.to_new_order()
            .ok_or(Error::BadInput("Malformed Order Request"))?;
        let user: User = users::table.find(author.id).first(conn)?;
        let portfolio: Portfolio = Portfolio::belonging_to(&user).first(conn)?;

        let candidates: Vec<Order> = match new.side {
            Side::Bid => orders::table
                .filter(
                    orders::claim_id
                        .eq(new.claim_id)
                        .and(orders::side.eq(Side::Ask))
                        .and(orders::num_shares.ge(new.num_shares))
                        .and(orders::user_id.ne(new.user_id)),
                )
                .order_by(orders::price.asc())
                .then_order_by(orders::created_at.desc())
                .get_results(conn)?,
            Side::Ask => orders::table
                .filter(
                    orders::claim_id
                        .eq(new.claim_id)
                        .and(orders::side.eq(Side::Bid))
                        .and(orders::num_shares.ge(new.num_shares))
                        .and(orders::user_id.ne(new.user_id)),
                )
                .order_by(orders::price.desc())
                .then_order_by(orders::created_at.desc())
                .get_results(conn)?,
        };

        let match_info = match new.side {
            Side::Bid => match_buy(&new, &candidates),
            Side::Ask => {
                let seller_coupon: Option<Coupon> = coupons::table
                    .filter(coupons::user_id.eq(author.id))
                    .get_result(conn)
                    .optional()?;
                match_sell(&new, &candidates, seller_coupon)
            }
        }?;

        let matching_order = match match_info {
            MatchInfo::NoMatch(lock_amt) => {
                info!(log, "no match; locking funds"; "funds" => lock_amt.to_string());

                let order = conn.transaction::<Order, Error, _>(|| {
                    let no: Order = diesel::insert_into(orders::table)
                        .values(&new)
                        .get_result(conn)?;

                    let new_lock = NewLock {
                        order_id: no.id,
                        portfolio_id: portfolio.id,
                        amount: lock_amt,
                    };

                    diesel::insert_into(locks::table)
                        .values(&new_lock)
                        .execute(conn)?;

                    Ok(no)
                })?;

                return Ok(OrderRes::Pending(order));
            }
            MatchInfo::Match(o) => o,
        };

        let (buyer_id, seller_id) = match new.side {
            Side::Bid => (new.user_id, matching_order.user_id),
            Side::Ask => (matching_order.user_id, new.user_id),
        };

        let (buyer_port, seller_port, buyer_coupons, seller_coupons): (
            Portfolio,
            Portfolio,
            Option<Coupon>,
            Option<Coupon>,
        ) = {
            let bp = portfolios::table
                .filter(portfolios::user_id.eq(buyer_id))
                .get_result(conn)?;
            let bc = coupons::table
                .filter(
                    coupons::user_id
                        .eq(buyer_id)
                        .and(coupons::claim_id.eq(new.claim_id)),
                )
                .get_result(conn)
                .optional()?;
            let sp = portfolios::table
                .filter(portfolios::user_id.eq(seller_id))
                .get_result(conn)?;
            let sc = coupons::table
                .filter(
                    coupons::user_id
                        .eq(seller_id)
                        .and(coupons::claim_id.eq(new.claim_id)),
                )
                .get_result(conn)
                .optional()?;

            (bp, sp, bc, sc)
        };

        let (buyer_locked, seller_locked) = {
            let bl: Option<BigDecimal> = locks::table
                .filter(locks::portfolio_id.eq(buyer_port.id))
                .select(sum(locks::amount))
                .first::<Option<BigDecimal>>(conn)?;
            let sl: Option<BigDecimal> = locks::table
                .filter(locks::portfolio_id.eq(seller_port.id))
                .select(sum(locks::amount))
                .first(conn)?;

            (bl, sl)
        };

        let outcome = calc_buy(
            &new,
            &matching_order,
            &buyer_port,
            buyer_locked,
            &seller_port,
            seller_locked,
            buyer_coupons.as_ref(),
            seller_coupons.as_ref(),
        )?;

        let handle_change = |conn: &PgConnection,
                             uid: i32,
                             portfolio: Portfolio,
                             claim_id: i32,
                             side: Side,
                             info: AcctChange,
                             lock: Option<Lock>,
                             coupon: Option<Coupon>|
         -> Result<()> {
            // Adjust balance and locked cash
            let loc = portfolios::table.filter(portfolios::user_id.eq(uid));
            diesel::update(loc)
                .set(portfolios::balance.eq(&portfolio.balance + &info.balance))
                .execute(conn)?;

            // Delete lock if applicable
            if let Some(lock) = lock {
                diesel::delete(&lock).execute(conn)?;
            }

            // Adjust coupons
            match info.coupons {
                CouponOutcome::Delete => {
                    let loc = coupons::table
                        .filter(coupons::user_id.eq(uid).and(coupons::claim_id.eq(claim_id)));
                    diesel::delete(loc).execute(conn)?;
                }
                CouponOutcome::Create(amt) => {
                    let loc = coupons::table;

                    let nc = NewCoupon {
                        user_id: uid,
                        claim_id: claim_id,
                        shares: amt,
                        side: side,
                    };

                    diesel::insert_into(loc).values(&nc).execute(conn)?;
                }
                CouponOutcome::Change(amt) => {
                    let coupon = coupon.unwrap();
                    diesel::update(&coupon)
                        .set(coupons::shares.eq(coupon.shares + amt))
                        .execute(conn)?;
                }
                CouponOutcome::Nothing => {}
            };

            Ok(())
        };

        let (buyer_lock, seller_lock) = {
            let bl: Option<Lock> = locks::table
                .filter(
                    locks::portfolio_id
                        .eq(buyer_port.id)
                        .and(locks::order_id.eq(matching_order.id)),
                )
                .first(conn)
                .optional()?;
            let sl: Option<Lock> = locks::table
                .filter(
                    locks::portfolio_id
                        .eq(seller_port.id)
                        .and(locks::order_id.eq(matching_order.id)),
                )
                .first(conn)
                .optional()?;

            (bl, sl)
        };

        info!(self.log, "executing transaction"; "info" => format!("{:?}", &outcome));

        let trade: Trade = conn.transaction::<Trade, Error, _>(|| {
            handle_change(
                &conn,
                buyer_id,
                buyer_port,
                new.claim_id,
                Side::Bid,
                outcome.buyer,
                buyer_lock,
                buyer_coupons,
            )?;
            handle_change(
                &conn,
                seller_id,
                seller_port,
                new.claim_id,
                Side::Ask,
                outcome.seller,
                seller_lock,
                seller_coupons,
            )?;

            // Remove matching order from orderbook
            diesel::delete(&matching_order).execute(conn)?;

            // Create trade record
            let trade = diesel::insert_into(trades::table)
                .values(&outcome.trade)
                .get_result(conn)?;

            Ok(trade)
        })?;

        Ok(OrderRes::Complete(trade))
    }
}

#[derive(Debug)]
pub enum MatchInfo {
    NoMatch(BigDecimal),
    Match(Order),
}

fn match_buy(new: &NewOrder, candidates: &[Order]) -> Result<MatchInfo> {
    let mut matching_order = None;

    for candidate in candidates {
        if candidate.price <= new.price && candidate.num_shares >= new.num_shares {
            matching_order = Some(candidate.clone());
        }
    }

    if matching_order.is_none() {
        let lock_amt = &new.price * BigDecimal::from(new.num_shares);

        return Ok(MatchInfo::NoMatch(lock_amt));
    }

    let mo = matching_order.unwrap();

    Ok(MatchInfo::Match(mo))
}

#[derive(Debug)]
pub enum CouponOutcome {
    Delete,
    Nothing,
    Change(i32),
    Create(i32),
}

#[derive(Debug)]
pub struct AcctChange {
    balance: BigDecimal,
    coupons: CouponOutcome,
}

#[derive(Debug)]
pub struct IOutcome {
    buyer: AcctChange,
    seller: AcctChange,
    trade: NewTrade,
}

fn calc_buy(
    new: &NewOrder,
    mo: &Order,
    buyer_port: &Portfolio,
    buyer_locked: Option<BigDecimal>,
    seller_port: &Portfolio,
    seller_locked: Option<BigDecimal>,
    buyer_coupons: Option<&Coupon>,
    seller_coupons: Option<&Coupon>,
) -> Result<IOutcome> {
    let (buyer_price, buyer_id, seller_price, seller_id, buy_amt) = if new.side == Side::Bid {
        (
            new.price.clone(),
            new.user_id,
            mo.price.clone(),
            mo.user_id,
            new.num_shares,
        )
    } else {
        (
            mo.price.clone(),
            mo.user_id,
            new.price.clone(),
            new.user_id,
            mo.num_shares,
        )
    };
    let claim_id = new.claim_id;
    let (buyer_locked, seller_locked) = (
        buyer_locked.unwrap_or_default(),
        seller_locked.unwrap_or_default(),
    );

    let buyer_pays: BigDecimal = &buyer_price * &BigDecimal::from(buy_amt);

    // Check buyer funds
    if buyer_pays > &buyer_port.balance - &buyer_locked {
        return Err(Error::AccountBalance(
            "Insufficient funds available for purchase".into(),
        ));
    }

    let (seller_coupon_info, trans) = match seller_coupons {
        Some(coupon) => {
            let diff = buy_amt - coupon.shares;

            if diff == 0 {
                (CouponOutcome::Delete, buyer_pays.clone())
            } else if diff < 0 {
                (CouponOutcome::Change(diff), buyer_pays.clone())
            } else {
                panic!("This should be unreachable, not enough coupons");
            }
        }
        None => {
            let seller_pays = (BigDecimal::from(100) - seller_price) * BigDecimal::from(buy_amt);

            if seller_pays > &seller_port.balance - &seller_locked {
                return Err(Error::AccountBalance(
                    "Insufficient funds available for unbacked sale".into(),
                ));
            }

            (CouponOutcome::Nothing, seller_pays.clone())
        }
    };

    let buyer_coupon_info = match buyer_coupons {
        Some(_) => CouponOutcome::Change(buy_amt),
        None => CouponOutcome::Create(buy_amt),
    };

    let ret = IOutcome {
        buyer: AcctChange {
            balance: -buyer_pays.clone(),
            coupons: buyer_coupon_info,
        },
        seller: AcctChange {
            balance: -trans,
            coupons: seller_coupon_info,
        },
        trade: NewTrade {
            buyer: buyer_id,
            seller: seller_id,
            claim_id: claim_id,
            price: buyer_price,
            shares: buy_amt,
        },
    };

    Ok(ret)
}

fn match_sell(new: &NewOrder, candidates: &[Order], coupon: Option<Coupon>) -> Result<MatchInfo> {
    let mut matching_order = None;

    for candidate in candidates {
        if candidate.price >= new.price && candidate.num_shares >= new.num_shares {
            matching_order = Some(candidate.clone());
        }
    }

    if matching_order.is_none() {
        let lock_amt = (BigDecimal::from(100) - &new.price)
            * BigDecimal::from(max(
                0,
                new.num_shares - coupon.map(|c| c.shares).unwrap_or(0),
            ));

        return Ok(MatchInfo::NoMatch(lock_amt));
    }

    let mo = matching_order.unwrap();

    Ok(MatchInfo::Match(mo))
}

#[cfg(test)]
mod test {
    use chrono::prelude::Utc;

    use super::*;

    use model::{Order, Portfolio, User};

    fn sample() -> (User, User, Portfolio, Portfolio, Vec<Order>) {
        let uid = 1;
        let claim_id = 1;
        let oid = 0x343;

        let u = User {
            id: uid,
            username: "pho".to_owned(),
            password: "password".to_string(),
            promoter: None,
            created_at: Utc::now(),
            role_id: 3,
        };

        let o = User {
            id: oid,
            username: "phi".to_owned(),
            password: "password".to_string(),
            promoter: None,
            created_at: Utc::now(),
            role_id: 3,
        };

        let p = Portfolio {
            id: 1,
            user_id: uid,
            balance: BigDecimal::from(1_000_000),
        };

        let op = Portfolio {
            id: 2,
            user_id: oid,
            balance: BigDecimal::from(1_000_000),
        };

        let orders = vec![
            Order {
                id: 1,
                user_id: uid + 1,
                side: Side::Ask,
                claim_id: claim_id,
                price: BigDecimal::from(70),
                num_shares: 100,
                created_at: Utc::now(),
                rank: 1,
            },
            Order {
                id: 2,
                user_id: uid + 2,
                side: Side::Bid,
                claim_id: claim_id,
                price: BigDecimal::from(30),
                num_shares: 100,
                created_at: Utc::now(),
                rank: 1,
            },
            Order {
                id: 3,
                user_id: oid,
                side: Side::Ask,
                claim_id: claim_id,
                price: BigDecimal::from(60),
                num_shares: 100,
                created_at: Utc::now(),
                rank: 1,
            },
        ];

        (u, o, p, op, orders)
    }

    #[test]
    fn matching_buy() {
        let (user, _, portfolio, o_port, candidates) = sample();

        let new_order = NewOrder {
            user_id: user.id,
            side: Side::Bid,
            claim_id: candidates[0].claim_id,
            price: BigDecimal::from(80),
            num_shares: 100,
            rank: 1,
        };

        let mi = match_buy(&new_order, &candidates).expect("Matching failed");

        if let MatchInfo::Match(o) = mi {
            let outcome = calc_buy(&new_order, &o, &portfolio, None, &o_port, None, None, None)
                .expect("Outcome calculation failed");

            assert_eq!(outcome.buyer.balance, BigDecimal::from(-100 * 80));
        } else {
            panic!("No Match when there should be one!");
        }
    }

    #[test]
    fn matching_buy_same_price() {
        let (user, _, portfolio, o_port, candidates) = sample();

        let new_order = NewOrder {
            user_id: user.id,
            side: Side::Bid,
            claim_id: candidates[0].claim_id,
            price: BigDecimal::from(70),
            num_shares: 100,
            rank: 1,
        };

        let mi = match_buy(&new_order, &candidates).expect("Matching failed");

        if let MatchInfo::Match(o) = mi {
            let outcome = calc_buy(&new_order, &o, &portfolio, None, &o_port, None, None, None)
                .expect("Outcome calculation failed");

            assert_eq!(outcome.buyer.balance, BigDecimal::from(-100 * 70));
        } else {
            panic!("No Match when there should be one!");
        }
    }

    #[test]
    fn matching_sell_unbacked() {
        let (user, _, portfolio, o_port, candidates) = sample();

        let new_order = NewOrder {
            user_id: user.id,
            side: Side::Ask,
            claim_id: candidates[0].claim_id,
            price: BigDecimal::from(20),
            num_shares: 100,
            rank: 1,
        };

        let mi = match_sell(&new_order, &candidates, None).expect("Matching failed");

        if let MatchInfo::Match(o) = mi {
            let outcome = calc_buy(&new_order, &o, &portfolio, None, &o_port, None, None, None)
                .expect("Outcome calculation failed");

            assert_eq!(
                outcome.seller.balance.to_string(),
                BigDecimal::from(-100 * 80).to_string()
            );
        } else {
            panic!("No Match when there should be one!");
        }
    }

    #[test]
    fn locking_buy() {
        let (user, _, _, _, candidates) = sample();

        let new_order = NewOrder {
            user_id: user.id,
            side: Side::Bid,
            claim_id: candidates[0].claim_id,
            price: BigDecimal::from(10),
            num_shares: 100,
            rank: 1,
        };

        let mi = match_buy(&new_order, &candidates).expect("Matching failed");

        if let MatchInfo::NoMatch(lock_amt) = mi {
            assert_eq!(BigDecimal::from(1_000), lock_amt);
        } else {
            panic!("Match present when there shouldn't be one!");
        }
    }
}

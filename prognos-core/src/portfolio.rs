use actix::prelude::*;
use diesel::prelude::*;

use model::Portfolio;
use Result;

use super::Core;

#[derive(Debug, Clone)]
pub struct Fetch(pub i32);

impl Message for Fetch {
    type Result = Result<Portfolio>;
}

impl Handler<Fetch> for Core {
    type Result = Result<Portfolio>;

    fn handle(&mut self, f: Fetch, _: &mut Self::Context) -> Result<Portfolio> {
        use schema::portfolios::dsl::*;

        let conn = self.conn()?;
        let p: Portfolio = portfolios.find(f.0).first(&conn)?;
        Ok(p)
    }
}

use actix::prelude::*;
use diesel::prelude::*;

use model::{Coupon, Order, Portfolio, Trade, User};
use Result;

use super::Core;

#[derive(Debug, Clone)]
pub struct Fetch(pub i32);

#[derive(Debug, Clone)]
pub struct Orders(pub i32);

#[derive(Debug, Clone)]
pub struct Coupons {
    pub id: i32,
}

#[derive(Debug, Clone)]
pub struct Trades(pub i32);

#[derive(Debug, Clone)]
pub struct Get {
    pub id: i32,
    pub portfolio: bool,
    pub promoter: bool,
}

#[derive(Debug, Clone)]
pub struct GetResult {
    pub user: User,
    pub promoter: Option<User>,
    pub portfolio: Option<Portfolio>,
}

impl Message for Fetch {
    type Result = Result<User>;
}

impl Message for Orders {
    type Result = Result<Vec<Order>>;
}

impl Message for Coupons {
    type Result = Result<Vec<Coupon>>;
}

impl Message for Trades {
    type Result = Result<Vec<Trade>>;
}

impl Message for Get {
    type Result = Result<GetResult>;
}

impl Handler<Fetch> for Core {
    type Result = Result<User>;

    fn handle(&mut self, f: Fetch, _: &mut Self::Context) -> Result<User> {
        use schema::users::dsl::*;

        let conn = self.conn()?;
        let u: User = users.find(f.0).first(&conn)?;
        Ok(u)
    }
}

impl Handler<Orders> for Core {
    type Result = Result<Vec<Order>>;

    fn handle(&mut self, msg: Orders, _: &mut Self::Context) -> Result<Vec<Order>> {
        use schema::orders;

        let conn = self.conn()?;
        let orders = orders::table
            .filter(orders::user_id.eq(msg.0))
            .get_results(&conn)?;

        Ok(orders)
    }
}

impl Handler<Coupons> for Core {
    type Result = Result<Vec<Coupon>>;

    fn handle(&mut self, msg: Coupons, _: &mut Self::Context) -> Result<Vec<Coupon>> {
        use schema::coupons;

        let conn = self.conn()?;
        let data = coupons::table
            .filter(coupons::user_id.eq(msg.id))
            .load(&conn)?;

        Ok(data)
    }
}

impl Handler<Trades> for Core {
    type Result = Result<Vec<Trade>>;

    fn handle(&mut self, msg: Trades, _: &mut Self::Context) -> Result<Vec<Trade>> {
        use schema::trades;

        let conn = self.conn()?;
        let data = trades::table
            .filter(trades::buyer.eq(msg.0).or(trades::seller.eq(msg.0)))
            .load(&conn)?;

        Ok(data)
    }
}

impl Handler<Get> for Core {
    type Result = Result<GetResult>;

    fn handle(&mut self, msg: Get, _: &mut Self::Context) -> Result<GetResult> {
        use schema::users::dsl::*;

        let conn = self.conn()?;

        let user: User = users.find(msg.id).first(&conn)?;
        let promo: Option<User> = if msg.promoter {
            match user.promoter {
                Some(pid) => Some(users.find(pid).first(&conn)?),
                None => None,
            }
        } else {
            None
        };
        let portfolio: Option<Portfolio> = if msg.portfolio {
            Some(Portfolio::belonging_to(&user).first(&conn)?)
        } else {
            None
        };

        Ok(GetResult {
            user,
            portfolio,
            promoter: promo,
        })
    }
}

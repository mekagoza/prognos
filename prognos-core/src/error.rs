use actix::MailboxError;
use chrono;
use diesel;
use r2d2;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "Database Pool Error: {}", _0)]
    R2d2(String), // added #[cause] attribute
    #[fail(display = "Database Error: {}", _0)]
    DbError(#[cause] diesel::result::Error),
    #[fail(display = "Date-time parse error: {}", _0)]
    Chrono(#[cause] chrono::ParseError),
    #[fail(display = "Actix Error: {}", _0)]
    Actix(String),
    #[fail(display = "Unauthorized: {}", _0)]
    Unauthorized(String),
    #[fail(display = "Account Balance Error: {}", _0)]
    AccountBalance(String),
    #[fail(display = "Bad Client Data: {}", _0)]
    BadInput(&'static str),
}

impl Error {}

impl From<diesel::r2d2::Error> for Error {
    fn from(inner: diesel::r2d2::Error) -> Self {
        Error::R2d2(inner.to_string())
    }
}

impl From<r2d2::Error> for Error {
    fn from(inner: r2d2::Error) -> Self {
        Error::R2d2(inner.to_string())
    }
}

impl From<diesel::result::Error> for Error {
    fn from(error: diesel::result::Error) -> Self {
        Error::DbError(error)
    }
}

impl From<chrono::ParseError> for Error {
    fn from(error: chrono::ParseError) -> Self {
        Error::Chrono(error)
    }
}

impl From<MailboxError> for Error {
    fn from(error: MailboxError) -> Self {
        Error::Actix(error.to_string())
    }
}

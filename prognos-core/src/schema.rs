table! {
    claims (id) {
        id -> Int4,
        name -> Text,
        expires -> Timestamptz,
        description -> Text,
        creator -> Int4,
        approved -> Bool,
        result -> Nullable<Bool>,
        bday -> Timestamptz,
    }
}

table! {
    coupons (id) {
        id -> Int4,
        user_id -> Int4,
        claim_id -> Int4,
        shares -> Int4,
        side -> Int4,
    }
}

table! {
    locks (id) {
        id -> Int4,
        portfolio_id -> Int4,
        order_id -> Int4,
        amount -> Numeric,
    }
}

table! {
    orders (id) {
        id -> Int4,
        user_id -> Int4,
        side -> Int4,
        claim_id -> Int4,
        price -> Numeric,
        num_shares -> Int4,
        created_at -> Timestamptz,
        rank -> Int4,
    }
}

table! {
    portfolios (id) {
        id -> Int4,
        user_id -> Int4,
        balance -> Numeric,
    }
}

table! {
    roles (id) {
        id -> Int4,
        name -> Text,
    }
}

table! {
    trades (id) {
        id -> Int4,
        buyer -> Int4,
        seller -> Int4,
        claim_id -> Int4,
        price -> Numeric,
        shares -> Int4,
        created_at -> Timestamptz,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Text,
        password -> Text,
        promoter -> Nullable<Int4>,
        created_at -> Timestamptz,
        role_id -> Int4,
    }
}

joinable!(claims -> users (creator));
joinable!(coupons -> claims (claim_id));
joinable!(coupons -> users (user_id));
joinable!(locks -> orders (order_id));
joinable!(locks -> portfolios (portfolio_id));
joinable!(orders -> claims (claim_id));
joinable!(orders -> users (user_id));
joinable!(portfolios -> users (user_id));
joinable!(trades -> claims (claim_id));
joinable!(users -> roles (role_id));

allow_tables_to_appear_in_same_query!(
    claims,
    coupons,
    locks,
    orders,
    portfolios,
    roles,
    trades,
    users,
);

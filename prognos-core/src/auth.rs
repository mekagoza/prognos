use actix::prelude::*;
use bigdecimal::BigDecimal;
use diesel::prelude::*;
use diesel::{self, Connection};

use model::{NewPortfolio, NewUser, Portfolio, User};
use {Error, Result};

use super::Core;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct UserForm {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct RegisterForm {
    pub username: String,
    pub promoter: String,
    pub password: String,
    pub confirm: String,
}

pub struct Delete(pub i32);

impl Message for UserForm {
    type Result = Result<User>;
}

impl Message for RegisterForm {
    type Result = Result<(User, Portfolio)>;
}

impl Message for Delete {
    type Result = Result<()>;
}

impl Handler<UserForm> for Core {
    type Result = Result<User>;

    fn handle(&mut self, login_user: UserForm, _: &mut Self::Context) -> Result<User> {
        use diesel::dsl::sql;
        use schema::users::dsl::*;

        let conn = self.conn()?;

        let (name, pass) = (&login_user.username, &login_user.password);
        let user_res: Option<User> = users
            .filter(
                username
                    .eq(name)
                    .and(sql(&format!("password = crypt('{}', password)", pass))),
            )
            .get_result(&conn)
            .optional()?;

        let u: User = match user_res {
            Some(x) => x,
            None => {
                error!(self.log, "error authorizing user");
                return Err(Error::Unauthorized("Username/Password Unknown".into()));
            }
        };

        Ok(u)
    }
}

impl Handler<RegisterForm> for Core {
    type Result = Result<(User, Portfolio)>;

    fn handle(
        &mut self,
        signup_user: RegisterForm,
        _: &mut Self::Context,
    ) -> Result<(User, Portfolio)> {
        use schema::portfolios;
        use schema::users::dsl::*;

        let log = self.log.new(o!("username" => signup_user.username.clone()));

        if signup_user.password != signup_user.confirm {
            return Err(Error::BadInput("password and password conf don't match"));
        }

        info!(
            log,
            "promoter is {promoter}",
            promoter = signup_user.promoter.clone()
        );

        let conn = self.conn()?;

        let promoter_name = &signup_user.promoter;

        let promo_id = users
            .select(id)
            .filter(username.eq(promoter_name))
            .limit(1)
            .get_result::<i32>(&conn)
            .ok();

        let new_user = NewUser {
            promoter: promo_id,
            username: &signup_user.username,
            password: &signup_user.password,
            role_id: 3,
        };

        let (u, p): (User, Portfolio) = conn.transaction::<(User, Portfolio), Error, _>(|| {
            let user: User = diesel::insert_into(users)
                .values(&new_user)
                .get_result(&conn)?;

            let new_portfolio = NewPortfolio {
                user_id: user.id,
                balance: BigDecimal::from(1_000_000),
            };
            let pf: Portfolio = diesel::insert_into(portfolios::table)
                .values(&new_portfolio)
                .get_result(&conn)?;

            Ok((user, pf))
        })?;

        Ok((u, p))
    }
}

impl Handler<Delete> for Core {
    type Result = Result<()>;

    fn handle(&mut self, msg: Delete, _: &mut Self::Context) -> Result<()> {
        use schema::users::dsl::*;

        let conn = self.conn()?;
        diesel::delete(users.find(msg.0)).execute(&conn)?;

        Ok(())
    }
}

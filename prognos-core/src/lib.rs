extern crate actix;
extern crate bigdecimal;
extern crate chrono;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate failure;
extern crate r2d2;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate slog;

pub mod auth;
pub mod claim;
pub mod error;
pub mod model;
pub mod order;
pub mod portfolio;
pub mod schema;
pub mod user;

pub use self::error::Error;

use actix::prelude::*;
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};
use slog::Logger;

/// This is db executor actor. We are going to run 3 of them in parallel.
pub struct Core {
    pool: PgPool,
    log: Logger,
}

pub type Result<T> = ::std::result::Result<T, Error>;

impl Core {
    pub fn new(logger: Logger, pool: PgPool) -> Core {
        Core { log: logger, pool }
    }

    pub fn conn(&self) -> Result<PooledConnection<ConnectionManager<PgConnection>>> {
        match self.pool.get() {
            Ok(conn) => Ok(conn),
            Err(e) => Err(e.into()),
        }
    }
}

impl Actor for Core {
    type Context = SyncContext<Self>;
}

pub type PgPool = Pool<ConnectionManager<PgConnection>>;

pub fn init_core(db_url: &str) -> Result<PgPool> {
    let manager = ConnectionManager::<PgConnection>::new(db_url);
    let pool = Pool::new(manager)?;
    Ok(pool)
}

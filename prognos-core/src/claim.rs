use actix::prelude::*;
use diesel;
use diesel::prelude::*;

use model::{Claim, NewClaim, Order, Trade};
use Result;

use super::Core;

#[derive(Debug, Clone)]
pub struct Fetch(pub i32);

#[derive(Debug, Clone)]
pub struct Create {
    pub name: String,
    pub description: String,
    pub expires: String,
    pub author: i32,
}

#[derive(Debug, Clone)]
pub struct Approve(pub i32);

#[derive(Debug, Clone)]
pub struct Orders(pub i32);

#[derive(Debug, Clone)]
pub struct Trades(pub i32);

#[derive(Debug, Clone, Deserialize)]
pub struct All;

impl Message for Fetch {
    type Result = Result<(Claim, String)>;
}

impl Message for Approve {
    type Result = Result<()>;
}

impl Message for Trades {
    type Result = Result<Vec<Trade>>;
}

impl Message for Orders {
    type Result = Result<Vec<Order>>;
}

impl Message for Create {
    type Result = Result<(Claim, String)>;
}

impl Message for All {
    type Result = Result<Vec<(Claim, String)>>;
}

impl Handler<Fetch> for Core {
    type Result = Result<(Claim, String)>;

    fn handle(&mut self, f: Fetch, _: &mut Self::Context) -> Result<(Claim, String)> {
        use schema::claims::dsl::*;
        use schema::users;

        let conn = self.conn()?;

        let (claim, creator_name) = claims
            .find(f.0)
            .inner_join(users::table)
            .select((claims::all_columns(), users::username))
            .first(&conn)?;

        Ok((claim, creator_name))
    }
}

impl Handler<Approve> for Core {
    type Result = Result<()>;

    fn handle(&mut self, msg: Approve, _: &mut Self::Context) -> Result<()> {
        use schema::claims::dsl::*;

        let conn = self.conn()?;

        diesel::update(claims.find(msg.0))
            .set(approved.eq(true))
            .execute(&conn)?;

        Ok(())
    }
}

impl Handler<All> for Core {
    type Result = Result<Vec<(Claim, String)>>;

    fn handle(&mut self, _: All, _: &mut Self::Context) -> Result<Vec<(Claim, String)>> {
        use schema::claims::dsl::*;
        use schema::users;

        let conn = self.conn()?;

        let thing = claims
            .inner_join(users::table)
            .select((claims::all_columns(), users::username))
            .load(&conn)?;
        Ok(thing)
    }
}

impl Handler<Trades> for Core {
    type Result = Result<Vec<Trade>>;

    fn handle(&mut self, msg: Trades, _: &mut Self::Context) -> Result<Vec<Trade>> {
        use schema::trades;

        let conn = self.conn()?;
        let data = trades::table
            .filter(trades::claim_id.eq(msg.0))
            .load(&conn)?;

        Ok(data)
    }
}

impl Handler<Orders> for Core {
    type Result = Result<Vec<Order>>;

    fn handle(&mut self, msg: Orders, _: &mut Self::Context) -> Result<Vec<Order>> {
        use schema::orders;

        let conn = self.conn()?;
        let orders = orders::table
            .filter(orders::claim_id.eq(msg.0))
            .get_results(&conn)?;

        Ok(orders)
    }
}

impl Handler<Create> for Core {
    type Result = Result<(Claim, String)>;

    fn handle(&mut self, msg: Create, _: &mut Self::Context) -> Result<(Claim, String)> {
        use schema::claims::dsl::*;
        use schema::users;

        let conn = self.conn()?;
        let exp_date = msg.expires.parse()?;

        let new_claim = NewClaim {
            name: &msg.name,
            creator: msg.author,
            description: &msg.description,
            expires: exp_date,
        };

        let claim: Claim = diesel::insert_into(claims)
            .values(&new_claim)
            .get_result(&conn)?;

        let author_name = users::table
            .find(claim.creator)
            .select(users::username)
            .first(&conn)?;

        info!(self.log, "created new claim"; "id" => claim.id, "name" => claim.name.clone());

        Ok((claim, author_name))
    }
}
